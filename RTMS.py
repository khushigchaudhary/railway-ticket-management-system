import os
import csv
import pandas as pd
import matplotlib.pyplot as plt
import tempfile

def display_train_schedule():
    print("                     Train Schedule            ")
    train_schedule = pd.read_csv("C:\\Users\\lenovo\\Desktop\\RTMS\\Train schedule.csv")
    print(train_schedule)

def check_availability_by_destination():
    print("              Availability(Destination wise)         ")
    destination = input("Enter Your Destination(Howrah/Chennai/Delhi/Lucknow/Amritsar/Bengaluru)=")
    with open("C:\\Users\\lenovo\\Desktop\\RTMS\\Train schedule.csv", 'r', newline='\r\n') as file:
        reader = csv.reader(file)
        for rec in reader:
            if rec[2] == destination:
                print("TRAIN NO=", rec[0])
                print("TRAIN NAME=", rec[1])
                print("DESTINATION=", rec[2])
                print("DURATION=", rec[3])

def book_ticket():
    print("                        Booking                   ")
    with open("C:\\Users\\lenovo\\Desktop\\RTMS\\List.csv", 'a', newline='\r\n') as file:
        writer = csv.writer(file)
        DESTINATION = input('Enter Your Destination(Delhi/Howrah/Chennai/Lucknow/Amritsar/Bengaluru)=')
        NAME = input('Enter Your Name=')
        AGE = int(input('Enter Your Age='))
        GENDER = input('Enter Your Gender(M/F)=')
        CLASS = input('Enter Your Seat Class (1AC/2AC/Sleeper)=')
        PHONE_NUMBER = int(input('Enter Your Mobile Number='))
        rec = [DESTINATION, NAME, AGE, GENDER, CLASS, PHONE_NUMBER]
        writer.writerow(rec)
        print("Successfully Booked")
        print("Further Details Will Be Sent To Your Mobile Number")

def cancel_booking():
    print("                       Cancellation        ")
    temp_dir = tempfile.gettempdir()
    temp_file_path = os.path.join(temp_dir, 'temp.csv')

    with open(temp_file_path, 'w', newline='\r\n') as temp_file, open('C:\\Users\\lenovo\\Desktop\\RTMS\\List.csv', 'r', newline='\r\n') as file:
        r = input('Enter Phone Number whose booking you want to cancel=')
        reader = csv.reader(file)
        writer = csv.writer(temp_file)

        for rec in reader:
            if rec[5] == r:
                print("Destination=", rec[0])
                print("Name=", rec[1])
                print("Class=", rec[4])
                print("Phone Number=", rec[5])
                choice = input("Do you want to cancel the booking(y/n)")
                if choice.lower() == 'y':
                    print("Booking Cancelled")
                else:
                    writer.writerow(rec)
            else:
                writer.writerow(rec)

    os.remove("C:\\Users\\lenovo\\Desktop\\RTMS\\List.csv")
    os.rename(temp_file_path, "C:\\Users\\lenovo\\Desktop\\RTMS\\List.csv")

def plot_train_destination():
    print("Graphical Representation(No. of trains destination wise)")
    Destination = ["Howrah", "Chennai", "Delhi", "Lucknow", "Amritsar", "Bengaluru"]
    Number = [2, 1, 2, 3, 1, 1]
    colors = ['pink', 'y', 'b', 'g', 'r', 'k']
    plt.bar(Destination, Number, color=colors)
    plt.xlabel("Destination")
    plt.ylabel("Number Of Trains")
    plt.title("Number Of Trains(Destination wise)")
    plt.grid(True)
    plt.yticks(Number)
    plt.show()

def main():
    print("****************MAIN MENU****************")
    print("1. Train Schedule")
    print("2. Check For Availability(Destination wise)")
    print("3. Booking")
    print("4. Cancellation")
    print("5. Graphical Representation(No. of trains destination wise)")
    print("6. Exit")
    
    c = int(input("Enter Your Choice (1, 2, 3, 4, 5, 6) = "))

    if c == 1:
        display_train_schedule()
    elif c == 2:
        check_availability_by_destination()
    elif c == 3:
        book_ticket()
    elif c == 4:
        cancel_booking()
    elif c == 5:
        plot_train_destination()
    elif c == 6:
        print("SOFTWARE TERMINATED")
    else:
        print("Enter Valid Choice")

if __name__ == "__main__":
    main()
