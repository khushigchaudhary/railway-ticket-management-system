##Title of the Project:
Railway Ticket Management System (RTMS)

##Problem Definition:
Design a Project to analyse the data set of Rail Travel Reservation and display Trains names available, Location of their departure and destination, Time of Arrival and Departure, Train Number and Name , Available Class and Berth ,Fair and Finalize a travel.

##Reason for choosing the Topic:
With the increase in the frequency of Rail travel it has become important to store all the details regarding the travel in a systematic way. This project will help the passengers reduce their time required in booking an efficient and reasonable ticket for passengers travelling. So we took the challenge and started working with this project to make the whole process more efficient as well as effective. 

##Objective:
To check the availability of trains from various locations.
To sort the available data on the basis of train name and number.
To check classes and berth available. 
To help reserving the ticket.

##Hardware $ Software Requirements:
 A Computer/Laptop with Operating System-Windows 7 or above x86 64-bit CPU (Intel / AMD architecture) 4 GB RAM. 5 GB free disk space.
Python 3.6.x or higher version (With Pandas Library preinstalled and Matplotlib Library preinstalled) - To run the program 
Ms-Office installed-To create the CSV files required

##Limitations:
It is not web based project 
Manual entry employees have more time.
More functionality can be added as per requirement.
Lack of accuracy and error prone.

##Bibliography:
Source of CSV  File- Referred to IRCTC portal to see working of railway management system.
Online Python documentation - for python command syntax 
Text book - Class XI and XII - Informatics Practices NCERT 
Google.com - For any online queries 

